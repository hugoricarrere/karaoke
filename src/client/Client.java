package client;

import config.Request;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client implements Closeable {

	private Socket socket;
	private BufferedInputStream bis;
	private BufferedOutputStream bos;

	public Client() {
	}

	public void connect(String host, int port) throws IOException {
		if (socket != null) {
			throw new IOException("Already connected");
		}
		socket = new Socket(host, port);
		bis = new BufferedInputStream(socket.getInputStream());
		bos = new BufferedOutputStream(socket.getOutputStream());
	}

	@Override
	public void close() throws IOException {
		if (socket != null) {
			socket.close();
		}
	}


	public void sendRequest(Request request) throws IOException {
//		request += "\r\n";
		bos.write(request.name().getBytes());
		bos.flush();
	}


	public String readResponse() throws IOException {
		StringBuilder content = new StringBuilder();
		int stream;
		byte[] b = new byte[1024];
		while((stream = bis.read(b)) != -1){
			content.append(new String(b, 0, stream));
		}
		return content.toString();
	}

	public int readUserInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
	}


//    public static void main(String[] args) {


//        try {
//            Socket s = new Socket("localhost", 1234);
//            InputStream is = s.getInputStream();
//            OutputStream os = s.getOutputStream();
//
//            Scanner scanner = new Scanner(System.in);
//            System.out.print("Choisissez la musique : "+"\n");
//
//            for (int i = 0; i < ListeMusiques.ListerRepertoire("C:/Users/nicolas/IdeaProjects/Karaoke/lyrics").size(); i++) {
//                System.out.println(i+" "+ ListeMusiques.ListerRepertoire("C:/Users/nicolas/IdeaProjects/Karaoke/lyrics").get(i).substring(0, ListeMusiques.ListerRepertoire("C:/Users/nicolas/IdeaProjects/Karaoke/lyrics").get(i).length()-4));
//            }
//
//            int num = scanner.nextInt();
//            os.write(num);
//
//            System.out.println("Choisissez les voix qui apparaitront : Homme (1), Femme (2), Choeur (3), Toutes (4)");
//            int voix = scanner.nextInt();
//            os.write(voix);
//
//            System.out.println("Lecture en cours : ");
//            new Fenetre(num,"C:/Users/nicolas/IdeaProjects/Karaoke/lyrics", voix);
//            s.close();
//
//        } catch (Exception e){
//
//            e.printStackTrace();
//        }

//    }

}
