package serveur;

import config.Config;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Closeable {

    private Socket socket;
    private ServerSocket serverSocket;
    private BufferedInputStream bis;
    private BufferedOutputStream bos;

    Server() {}

    public void connect(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void accept() throws IOException {
        socket = serverSocket.accept();
        bis = new BufferedInputStream(socket.getInputStream());
        bos = new BufferedOutputStream(socket.getOutputStream());
    }

    @Override
    public void close() throws IOException {
        if (socket != null) {
            socket.close();
        }
        if (serverSocket != null) {
            serverSocket.close();
        }
    }


    public static void main(String[] args) {
        try {
            // Création du serveur
            Server server = new Server();
            server.connect(Config.PORT);

            // Connexion du client
            server.accept();
            System.out.println("Un client vient de se connecter");


        } catch (IOException e) {
            System.err.println("Le port " + Config.PORT + " est déjà utilisé");
            e.printStackTrace();
        }


//        try {
//            ServerSocket ss = new ServerSocket(Config.PORT);
//            System.out.println("Liste des musiques disponibles :");
//
//            //importer depuis listemusiques.java
//
//            System.out.println("\nJ'attends la connexion du client");
//            Socket s = ss.accept();
//
//            InputStream is = s.getInputStream();
//            OutputStream os = s.getOutputStream();
//            /*ObjectOutputStream oos = new ObjectOutputStream(os);
//            oos.write();*/
//            System.out.println("\nJ'attends le choix du client");
//            int num = is.read();
//            System.out.println("\nJ'envoie la réponse");
//            os.write(num);
//            s.close();
//
//        } catch (IOException e) {
//            //Auto generate catch block
//            e.printStackTrace();
//        }
    }
}