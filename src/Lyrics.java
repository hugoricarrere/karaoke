import javax.swing.*;
import java.awt.*;
import java.io.*;

import static java.awt.Color.*;
import static java.awt.Color.BLACK;

public class Lyrics {

    public static String lireLyrics(int num, String text) throws IOException {
        File file = new File(text+"/"+ListeMusiques.ChoixMusique(num, text));
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        JPanel pan = new JPanel();
        while ((line = br.readLine()) != null) {
            System.out.println(line);
            if (line.length()>12) {
                JLabel jlabel = new JLabel(line.substring(12));
                pan.add(jlabel);
                jlabel.setForeground(genre(line));
                return(line);
            }
        }
        return("merci");
    }

    public static Color genre(String line){
        if (line.charAt(10)=='h'){
            return CYAN;
        }
        else if (line.charAt(10)=='f'){
            return MAGENTA;
        }
        else if (line.charAt(10)=='c'){
            return GREEN;
        }
        return BLACK;
    }
}
