// Java program showing the implementation of a simple record

import javax.sound.midi.*;
import java.io.File;
import java.io.IOException;


public class Midi {

    //Partie main a ajouter à la classe client

    public static void main(String[] args) throws IOException, MidiUnavailableException, InvalidMidiDataException {

        Sequence sequence;

        //File Path a remplacer par l'objet file envoyer par le serveur
        sequence = MidiSystem.getSequence(new File("C:/Users/nicolas/IdeaProjects/Karaoke/midiFiles/Gotye - Somebody That I Used To Know.mid"));

        // Create a sequencer for the sequence
        Sequencer sequencer = MidiSystem.getSequencer();
        sequencer.open();
        sequencer.setSequence(sequence);
        /*sequencer.addMetaEventListener(new MetaEventListener() {
            @Override
            public void meta(MetaMessage metaMessage) {
                metaMessage.getData()
            }
        })*/
        // Start playing
        sequencer.start();

        //Ajouter des methode pour faire pause un retour arriere etc ..

    }
}

