package model;

public class Statistics {

	private int numberOfListenings;
	private String song;

	public Statistics(int numberOfListenings, String song) {
		this.numberOfListenings = numberOfListenings;
		this.song = song;
	}

	public int getNumberOfListenings() {
		return numberOfListenings;
	}

	public void setNumberOfListenings(int numberOfListenings) {
		this.numberOfListenings = numberOfListenings;
	}

	public String getSong() {
		return song;
	}

	public void setSong(String song) {
		this.song = song;
	}
}
