import java.io.*;
import java.util.ArrayList;

public class Musique {

    public static ArrayList<Long> lectureParole(int num, String text) throws IOException {
        ArrayList<Long> valeur= new ArrayList<>();
        File file = new File(text+"/"+ListeMusiques.ChoixMusique(num, text));
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while((line = br.readLine()) != null){
            long i=traitementTemps(line);
            valeur.add(i);
        }

        return valeur;
    }

    public static int traitementTemps(String ligne) throws IOException {
        //String temps = ligne.substring(2,10);

        int s;
        int min;
        //System.out.println("temps: "+temps);
        min = Integer.parseInt(ligne.substring(2,4));
        System.out.println(min);
        s = Integer.parseInt(ligne.substring(5,7) + ligne.substring(8,10    ))*10 ;
        //System.out.println(min+s);
        return min+s;
    }
}

