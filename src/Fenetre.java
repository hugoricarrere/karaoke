import javax.swing.*;
import java.awt.*;
import java.io.*;

import static java.awt.Color.*;

public class Fenetre extends JFrame {

    public Fenetre(int num, String text, int voix) throws IOException {
        this.setTitle(ListeMusiques.ChoixMusique(num, text));
        System.out.println(ListeMusiques.ChoixMusique(num, text));
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);

        Thread t = new Thread();
        JPanel pan = new JPanel();
        Musique.lectureParole(num, text);
        File file = new File(text+"/"+ListeMusiques.ChoixMusique(num, text));
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;

        while ((line = br.readLine()) != null) {
            if (line.length()>12) {
                if (voix == 1){
                    if (line.charAt(10) == 'h'){
                        JLabel jlabel = new JLabel(line.substring(12));
                        jlabel.setForeground(genre(line));
                        pan.add(jlabel);
                    }
                }
                if (voix == 2){
                    if (line.charAt(10) == 'f'){
                        JLabel jlabel = new JLabel(line.substring(12));
                        jlabel.setForeground(genre(line));
                        pan.add(jlabel);
                    }
                }
                if (voix == 3){
                    if (line.charAt(10) == 'c'){
                        JLabel jlabel = new JLabel(line.substring(12));
                        jlabel.setForeground(genre(line));
                        pan.add(jlabel);
                    }
                }
                if (voix == 4) {
                    JLabel jlabel = new JLabel(line.substring(12));
                    jlabel.setForeground(genre(line));
                    pan.add(jlabel);
                }

            }
        }
        br.close();
        setContentPane(pan);
    }

    public Color genre(String line){
        if (line.charAt(10) == 'h') {
            return BLUE;
        } else if (line.charAt(10) == 'f') {
            return MAGENTA;
        } else if (line.charAt(10) == 'c') {
            return GREEN;
        }
        return BLACK;
    }
}