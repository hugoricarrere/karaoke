import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ListeMusiques {

    /*public ListeMusiques(String chemin){//constructeur contenant une liste d'objets (objets sont des musiques composé d'un fichier son et un de texte

    }*/

    static String ChoixMusique(int num, String chemin){
        return ListerRepertoire(chemin).get(num);
    };

    static List<String> ListerRepertoire(String chemin) {

        File repertoire = new File(chemin);
        String[] liste = repertoire.list();
        List<String> lvide = new ArrayList<String>();


        if (liste != null) {
            for (int i = 0; i < liste.length; i++) {
                lvide.add(liste[i]);
            }
        } else {
            System.err.println("Nom de repertoire invalide");
        }
        return lvide;
    }

}

