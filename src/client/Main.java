package client;

import config.Config;
import config.Request;
import mapper.SongMapper;
import model.Song;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

public class Main {

	public static void printSongs(List<Song> songs) {
		System.out.println("Liste des morceaux disponibles : ");
		for (int i=0 ; i<songs.size() ; i++) {
			System.out.println(i + " - " + songs.get(i).getName());
		}
		System.out.println("Choisissez le numéro d'un morceau, ou appuyez sur -1 pour quitter");
	}

	public static void main(String[] args) {
		try (Client client = new Client()) {
			// Connexion au serveur
			client.connect(Config.HOST_NAME, Config.PORT);
			System.out.println("Connexion établie avec le serveur");

			// Récupération et affichage de la liste des morceaux
			client.sendRequest(Request.GET_LIST_OF_SONGS);
			List<Song> songs = SongMapper.responseToSongList(client.readResponse());
			printSongs(songs);

			// Choix de l'utilisateur
			int userInput = client.readUserInput();




//			String request = "GET /wiki/Digital_Learning HTTP/1.1\r\nHost: fr.wikipedia.org\r\n\r\n";
//			client.sendRequest(request);
//			String content = client.readResponse();
//			System.out.println(content);
		} catch (UnknownHostException e) {
			System.err.println("Error : the host : " + Config.HOST_NAME + " is unknown.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Error : the port : " + Config.PORT + " is not open or already being used.");
			e.printStackTrace();
		}
	}

}